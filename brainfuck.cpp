#include "llvm/Analysis/Verifier.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/JIT.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/PassManager.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/system_error.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/Scalar.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace llvm;
using namespace std;

Function *compileBrainfuck(Module *module, const char *code, size_t size) {
  LLVMContext &ctx = module->getContext();
  IRBuilder<> irb(ctx);

  // Platform specific type
  IntegerType *int_ty = irb.getInt32Ty();
  IntegerType *ptr_int_ty = irb.getInt64Ty();

  // Declare "int getchar()" function prototype
  Function *getchar_func = module->getFunction("getchar");
  if (!getchar_func) {
    FunctionType *getchar_func_ty = FunctionType::get(int_ty, false);
    getchar_func = Function::Create(getchar_func_ty,
                                    GlobalValue::ExternalLinkage,
                                    "getchar", module);
  }

  // Declare "int putchar(int)" function prototype
  Function *putchar_func = module->getFunction("putchar");
  if (!putchar_func) {
    Type *putchar_func_arg_ty[] = { int_ty };
    FunctionType *putchar_func_ty =
      FunctionType::get(int_ty, putchar_func_arg_ty, false);
    putchar_func = Function::Create(putchar_func_ty,
                                    GlobalValue::ExternalLinkage,
                                    "putchar", module);
  }

  // Create a new function
  Type *func_arg_ty[] = { int_ty->getPointerTo() };
  FunctionType *func_ty =
    FunctionType::get(irb.getVoidTy(), func_arg_ty, false);
  Function *func = Function::Create(func_ty, GlobalValue::ExternalLinkage,
                                    "brainfuck", module);

  // Create basic block for each brainfuck opcode
  BasicBlock *entry = BasicBlock::Create(ctx, "entry", func);
  vector<BasicBlock *> bb(size + 1, NULL);
  for (size_t i = 0; i < size + 1; ++i) {
    bb[i] = BasicBlock::Create(ctx, "b" + Twine(i), func);
  }

  // Create entry basic block
  irb.SetInsertPoint(entry);
  Value *pos_addr = irb.CreateAlloca(ptr_int_ty);
  irb.CreateStore(ConstantInt::get(ptr_int_ty, 0), pos_addr);
  irb.CreateBr(bb[0]);

  // Create returning basic block
  irb.SetInsertPoint(bb[size]);
  irb.CreateRetVoid();

  // Translate each brainfuck opcode
  Value *tape_addr = func->arg_begin();
  vector<pair<size_t, Value *> > brackets;
  for (size_t i = 0; i < size; ++i) {
    irb.SetInsertPoint(bb[i]);
    switch (code[i]) {
    case '>':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *new_pos = irb.CreateAdd(pos, ConstantInt::get(ptr_int_ty, 1));
        irb.CreateStore(new_pos, pos_addr);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case '<':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *new_pos = irb.CreateSub(pos, ConstantInt::get(ptr_int_ty, 1));
        irb.CreateStore(new_pos, pos_addr);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case '+':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *ptr = irb.CreateGEP(tape_addr, pos);
        Value *val = irb.CreateLoad(ptr);
        Value *new_val = irb.CreateAdd(val, ConstantInt::get(int_ty, 1));
        irb.CreateStore(new_val, ptr);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case '-':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *ptr = irb.CreateGEP(tape_addr, pos);
        Value *val = irb.CreateLoad(ptr);
        Value *new_val = irb.CreateSub(val, ConstantInt::get(int_ty, 1));
        irb.CreateStore(new_val, ptr);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case '.':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *ptr = irb.CreateGEP(tape_addr, pos);
        Value *val = irb.CreateLoad(ptr);
        irb.CreateCall(putchar_func, val);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case ',':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *ptr = irb.CreateGEP(tape_addr, pos);
        Value *val = irb.CreateCall(getchar_func);
        irb.CreateStore(val, ptr);
        irb.CreateBr(bb[i + 1]);
      }
      break;

    case '[':
      {
        Value *pos = irb.CreateLoad(pos_addr);
        Value *ptr = irb.CreateGEP(tape_addr, pos);
        Value *val = irb.CreateLoad(ptr);
        Value *cond = irb.CreateICmpNE(val, ConstantInt::get(int_ty, 0));
        brackets.push_back(make_pair(i, cond));
      }
      break;

    case ']':
      {
        // Get the matching '['
        if (brackets.empty()) {
          cerr << "ERROR: Unmatching right bracket at " << i << endl;
          exit(EXIT_FAILURE);
        }

        size_t loop_entry = brackets.back().first;
        Value *cond = brackets.back().second;
        brackets.pop_back();

        // Branch to loop entry
        irb.CreateBr(bb[loop_entry]);

        // Finish the conditional branch for while-loop
        irb.SetInsertPoint(bb[loop_entry]);
        irb.CreateCondBr(cond, bb[loop_entry + 1], bb[i + 1]);
      }
      break;

    default:
      irb.CreateBr(bb[i + 1]);
      break;
    }
  }

  // Check the unmatched '['
  for (size_t i = 0; i < brackets.size(); ++i) {
    cerr << "ERROR: Unmatched left bracket at " << brackets[i].first << endl;
  }
  if (!brackets.empty()) {
    exit(EXIT_FAILURE);
  }

  // Verify the function
  verifyFunction(*func);
  return func;
}

int main(int argc, char **argv) {
  // Load the brainfuck source code
  string filename("-");
  if (argc >= 2) {
    filename = argv[1];
  }

  OwningPtr<MemoryBuffer> input;
  if (MemoryBuffer::getFileOrSTDIN(filename, input) != 0) {
    cerr << "ERROR: Failed to open file: " << argv[1] << endl;
    exit(EXIT_FAILURE);
  }

  // Initialize the native target backend
  InitializeNativeTarget();

  // Translate the code
  LLVMContext &ctx = getGlobalContext();
  Module *module = new Module("brainfuck-module", ctx);
  Function *main_func = compileBrainfuck(module,
                                         input->getBufferStart(),
                                         input->getBufferSize());

  // JIT execution engine
  string errstr;
  ExecutionEngine *engine = EngineBuilder(module).setErrorStr(&errstr).create();
  if (!errstr.empty()) {
    cout << "ERROR: " << errstr << endl;
    exit(EXIT_FAILURE);
  }

  // Optimize the generated function
  FunctionPassManager fpm(module);
  fpm.add(new DataLayout(*engine->getDataLayout()));

  PassManagerBuilder pmb;
  pmb.OptLevel = 3;
  pmb.populateFunctionPassManager(fpm);

  fpm.doInitialization();
  fpm.run(*main_func);
  //module->dump();

  // Compile and run just-in-time
  vector<int> tape(4096, 0);
  void (*compiled_main_func)(int *) =
    reinterpret_cast<void (*)(int *)>(
      engine->getPointerToFunction(main_func));
  compiled_main_func(&*tape.begin());

  return EXIT_SUCCESS;
}
