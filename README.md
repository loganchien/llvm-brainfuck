Simple Brainfuck to LLVM Compiler
=================================

This is a small Brainfuck-to-LLVM compiler which demostrates:

1. The concept of LLVM modules, functions, basic blocks, and instructions.

2. How to optimize the generated functions with LLVM passes.

3. How to run the generated functions with JIT execution engine.

4. How to build the LLVM IR which assigns to a variables multiple times
   but not build the SSA-form or inserting phi instructions by ourselves.


Practice
--------

This compiler is designed to be a simple example for tutorial.  It is
pretty inefficient.  Here are some practices for you!  :-)

1. The consecutive `+` and `-` can be constant-folded to one addition or
   substraction operation.  But due to our design, even the optimization passes
   from LLVM can't reduce them.

2. For simplexity, we are creating *many* unnecessary basic blocks.
   You may try to reduce the basic blocks by ignoring the comment characters
   or analyze the brackets `[` and `]` in the Brainfuck source code.
