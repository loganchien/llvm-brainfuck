CXXFLAGS := $(shell llvm-config --cxxflags)
LDFLAGS := $(shell llvm-config --ldflags)
LIBS := $(shell llvm-config --libs core jit native ipo)

all: brainfuck

brainfuck: brainfuck.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LIBS) $(LDFLAGS)

.PHONY: clean
clean:
	-rm brainfuck
